using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public GameObject cube;
    public GameObject text;

    public float moveSpeed;
    public float shrinkDelta = 0.5f;

    private int size;
    private int shrinkaudio;
    private int growaudio;

    private float nextShrink = 1f;
    private float myTime = 0.0f;

    public AudioSource growAudio;
    public AudioSource shrinkAudio;
    public AudioSource landAudio;

    private Vector3 scaleGrow;
    private Vector3 scaleShrink;

    void Start ()
    {
        text.SetActive(true);
        moveSpeed = 4f;
        size = 0;
        scaleGrow = new Vector3(3.5f, 3.5f, 3.5f);
        scaleShrink = new Vector3(0.3f, 0.3f, 0.3f);
    }

    void Update ()
    {
        myTime = myTime + Time.deltaTime;

        transform.Translate(moveSpeed*Input.GetAxis("Horizontal")*Time.deltaTime,0f,moveSpeed*Input.GetAxis("Vertical")*Time.deltaTime);

       if (Input.GetButton("Fire1") && myTime > nextShrink && size >= 0 && shrinkaudio == 0)
        {
            moveSpeed = 3.5f;
            shrinkAudio.Play();
            shrinkaudio = 1;
            size = size - 1;
            nextShrink = myTime + shrinkDelta;
            cube.transform.localScale = scaleShrink;
            nextShrink = nextShrink - myTime;
            myTime = 0.0f;
            growaudio = 0;
        }

        if (Input.GetButton("Fire3") && myTime > nextShrink && size <= 0 && growaudio == 0)
        {
            moveSpeed = 2f;
            growAudio.Play();
            growaudio = 1;
            size = size + 1;
            nextShrink = myTime + shrinkDelta;
            cube.transform.localScale = scaleGrow;
            nextShrink = nextShrink - myTime;
            myTime = 0.0f;
            shrinkaudio = 0;
        }

        if (Input.GetButton ("Cancel"))
        {
            Application.Quit();
        }
    }

    void OnCollisionEnter(Collision Ground)
    {
        landAudio.Play();
    }
}