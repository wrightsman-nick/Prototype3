using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickup : MonoBehaviour
{

    public GameObject capsule;
    public GameObject cube;

    public AudioSource pickupSound;

    bool m_atPickUp;

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject == cube)
        {
            m_atPickUp = true;
        }
    }

    void Update()
    {
        if(m_atPickUp)
        {
            pickupSound.Play();
            capsule.SetActive(false);
        }
    }
}
